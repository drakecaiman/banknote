//
//  TransactionDetailViewController.swift
//  banknote-ios
//
//  Created by Duncan Oliver on 8/24/16.
//  Copyright © 2016 Highjacks. All rights reserved.
//

import UIKit

//TODO: Switch storyboard to autoresizing?
/// The `TransactionDetailViewController` allows the user to view and edit the details of a specific transaction
class TransactionDetailViewController: UITableViewController
{
//  MARK: Constants
/// The segue identifier for transisting to the detail view of a transaction
    fileprivate let TRANSACTION_DETAIL_VIEW_SEGUE_IDENTIFIER_FINISH = "TransactionDetailFinishSegue"
/// `DirectionControlSegmentIndex` defines the meaning of each segment in `directionSegmentedControl`
    enum DirectionControlSegmentIndex : Int
    {
///     The segment for indicating a balance increase
        case increase
///     The segment for indicating a balance decrease
        case decrease
    }
//  MARK: Properties
/// The `TransactionController` object used for getting data for the currently display transaction
    fileprivate var transactionController = TransactionController()
//  MARK: Outlets
/// The view used to display and edit the name of the transaction
    @IBOutlet var nameTextField     : UITextField?
/// The view used to display and edit the category of the transaction
    @IBOutlet var categoryTextField : UITextField?
/// The view used to display and edit the terminus of the transaction
    @IBOutlet var terminusTextField : UITextField?
//  TODO: Update format after edit
/// The view used to display and edit the date of the transaction
    @IBOutlet var dateTextField     : UITextField?
/// The control used to change the date of the transaction
    @IBOutlet var transactionDatePicker: UIDatePicker?
//  TODO: Update format after edit
/// The view used to display and edit the amount of money transferred in the transaction
    @IBOutlet var amountTextField : UITextField?
/// The view used to display and edit the direction of money flow
    @IBOutlet var directionSegmentedControl : UISegmentedControl?
    
//  MARK: -
    /**
     Set the currently displayed/edited transaction for this view
     
     - parameter transaction: The transaction to display
     */
    func setTransaction(_ transaction: Transaction)
    {
        self.transactionController.transaction = transaction
    }
    
    /**
     Update the current transaction with the new values from user editing
     */
    func updateTransaction()
    {
//      Update transaction name
        if let newNameString = self.nameTextField?.text
        {
            self.transactionController.setName(newNameString)
        }
//      Update transaction category
        if let newCategoryString = self.categoryTextField?.text
        {
            self.transactionController.setCategory(newCategoryString)
        }
//      Update transaction terminus
        if let newTerminusString = self.terminusTextField?.text
        {
            self.transactionController.setTerminus(newTerminusString)
        }
//      Update transaction date
        if let newDateString = self.dateTextField?.text,
            let newDate = self.transactionController.getDateFormatter().date(from: newDateString)
        {
            self.transactionController.setDate(newDate)
        }
//      Update transaction value
        if let newAmountString = self.amountTextField?.text,
            let newAmount = self.transactionController.getCurrencyFormatter().number(from: newAmountString)?.floatValue
        {
            self.transactionController.transaction?.amount = newAmount
        }
        if let directionControl = self.directionSegmentedControl,
            let newDirectionControlSegmentIndex = DirectionControlSegmentIndex(rawValue: directionControl.selectedSegmentIndex)
        {
            let newDirection : Transaction.Direction
            switch newDirectionControlSegmentIndex
            {
            case .increase:
                newDirection = .increase
            case .decrease:
                newDirection = .decrease
            }
            self.transactionController.setDirection(newDirection)
        }
    }
    
    /**
     Update the value-relevant views with the transaction's info
     */
    fileprivate func updateValueDisplay()
    {
        self.amountTextField?.text  = self.transactionController.getAmountString()
        if let transactionDirection = self.transactionController.transaction?.direction
        {
            switch transactionDirection
            {
            case .increase:
                self.directionSegmentedControl?.selectedSegmentIndex = DirectionControlSegmentIndex.increase.rawValue
            case .decrease:
                self.directionSegmentedControl?.selectedSegmentIndex = DirectionControlSegmentIndex.decrease.rawValue
            }
        }
    }
    
//  MARK: Actions
    /**
     Apply changes from the view to the active transaction
     
     - parameter sender: The object sending the action
     */
    @IBAction func confirmDetailChanges(_ sender: AnyObject?)
    {
        updateTransaction()
        self.performSegue(withIdentifier: TRANSACTION_DETAIL_VIEW_SEGUE_IDENTIFIER_FINISH, sender: sender)
    }
    
    /**
     Update the display of date-related views based on the sender
     
     - parameter sender: The object sending the action
     */
    @IBAction func dateUpdated(_ sender: UIDatePicker?)
    {
        guard let datePicker = sender else { return }
        let dateFormatter = self.transactionController.getDateFormatter()
        self.dateTextField?.text = dateFormatter.string(from: datePicker.date)
    }
    
    /**
     Update the display of value-related views based on the sender
     
     - parameter sender: The object sending the action
     */
    @IBAction func valueUpdated(_ sender: AnyObject?)
    {
//      TODO: Why?
//        self.updateValueDisplay()
    }
    
//  MARK: UIViewController methods
    override func viewWillAppear(_ animated: Bool)
    {
        self.dateTextField?.inputView = transactionDatePicker
        
        self.nameTextField?.text        = self.transactionController.transaction?.name
        self.categoryTextField?.text    = self.transactionController.transaction?.category
        self.dateTextField?.text        = self.transactionController.getDateString()
        self.terminusTextField?.text    = self.transactionController.transaction?.terminus
        updateValueDisplay()
    }
}

