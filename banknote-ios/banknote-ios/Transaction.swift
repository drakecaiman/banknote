//
//  Transaction.swift
//  banknote-ios
//
//  Created by Duncan Oliver on 8/24/16.
//  Copyright © 2016 Highjacks. All rights reserved.
//

import Foundation

/// `Transaction` objects represent the change in total of a balance, defining things like when, by how much, and in what direction that change is.
class Transaction
{
//  MARK: Constants
/// `Direction` defines whether the transaction represents a influx or outlet of money for the balance
    enum Direction
    {
///     An increase in balance
        case increase
///     A decrease in balance
        case decrease
    }
//  MARK: Properties
/// The name of the transaction
    var name : String?
/// The category of the transaction
    var category : String?
/// The opposite endpoint from this account of this transaction
    var terminus : String?
//  TODO: Redo "direction" storage
/// The 'magnitude' of the change for this transaction
    var amount : Float = 0.00
    {
        didSet
        {
            if self.amount.sign == .minus
            {
                self.amount *= -1
            }
        }
    }
    
//  TODO: Get current currency code
/// The currency this transaction is expresented in
    var currency = "" // = NSLocale.currentLocale().valueForKey(NSLocaleCurrencyCode) as? String ?? ""
/// The direction of this transaction
    var direction : Direction = .decrease
/// The signed value of this transaction, representing its impact on the balance
    var value: Float
    {
        return amount * (self.direction == .increase || amount == 0 ? 1.0 : -1.0)
    }
//  TODO: Remove time component?
/// The date when this transaction occurred
    var date : Date = Date()
    
}

// TODO: Multi-part transactions?
//extension Transaction : ExpressibleByArrayLiteral
//{
////  TODO: For splits?
//    convenience init(arrayLiteral elements: Float...)
//    {
//    }
//}
