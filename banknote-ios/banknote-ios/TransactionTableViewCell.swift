//
//  TransactionTableViewCell.swift
//  banknote-ios
//
//  Created by Duncan Oliver on 1/4/17.
//  Copyright © 2017 Highjacks. All rights reserved.
//

import UIKit


class TransactionTableViewCell: UITableViewCell
{
    @IBOutlet var nameLabel     : UILabel?
    @IBOutlet var terminusLabel : UILabel?
    @IBOutlet var categoryLabel : UILabel?
    @IBOutlet var valueLabel    : UILabel?
}
