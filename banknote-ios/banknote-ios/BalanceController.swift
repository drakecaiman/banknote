//
//  BalanceController.swift
//  banknote-ios
//
//  Created by Duncan Oliver on 8/25/16.
//  Copyright © 2016 Highjacks. All rights reserved.
//

import Foundation

/// `BalanceController` provides transformed data based on the currently represented `Balance`.
class BalanceController
{
//  MARK: Properties
/// The current `Balance` controlled
    var balance : Balance?
    
//  MARK: Initializers
    init(with balance: Balance? = nil)
    {
        self.balance = balance
    }
    
//  -
    /**
     Adds a new `Transaction` object to the currently controlled `Balance`
     */
    func newTransaction()
    {
        let newTransaction = Transaction()
        self.balance?.transactions.append(newTransaction)
    }
    
    /**
     Get all of the transactions for the current `Balance`, order by their date
     
     - parameter isAscending: Specifies the direction of the returned array. `true` indicates the dates will go from earliest to lates. The default value is `true`.
     - returns: An ordered array of all the transactions for the current `Balance` or `nil` if there is no current `Balance`
     */
    func getTransactionsOrderedByDate(isAscending ascending: Bool = true) -> [Transaction]?
    {
        guard let balanceTransactions = self.balance?.transactions else { return nil }
        return self.sortTransactionsByDate(balanceTransactions, ascending: ascending)
    }
    
    /**
     Get all of the transactions for the current `Balance` that occurred before and including the current date, order by their date
     
     - parameter isAscending: Specifies the direction of the returned array. `true` indicates the dates will go from earliest to latest. The default value is `true`.
     - returns: An ordered array of all the transactions before and including today for the current `Balance` or `nil` if there is no current `Balance`
     */
    func getPastTransactionsOrderedByDate(isAscending ascending: Bool = true) -> [Transaction]?
    {
        guard let balanceTransactions = self.balance?.transactions else { return nil }
        let orderedTransactions = self.sortTransactionsByDate(balanceTransactions,
                                                              ascending: ascending)
        let now = Date()
        return self.getMatchingTransactions(orderedTransactions, beforeAndIncludingDate: now)
    }
    
    /**
     Get all of the transactions for the current `Balance` that occur after the current date, order by their date
     
     - parameter isAscending: Specifies the direction of the returned array. `true` indicates the dates will go from earliest to latest. The default value is `true`.
     - returns: An ordered array of all the transactions after today for the current `Balance` or `nil` if there is no current `Balance`
     */
    func getFutureTransactionsOrderedByDate(isAscending ascending: Bool = true) -> [Transaction]?
    {
        guard let balanceTransactions = self.balance?.transactions else { return nil }
        let orderedTransactions = self.sortTransactionsByDate(balanceTransactions,
                                                              ascending: ascending)
        let now = Date()
        return self.getMatchingTransactions(orderedTransactions, afterDate: now)
    }
    
    /**
     For a collection on `Transaction` objects, get all `Transactions` that occur before and including the current date
     
     - parameter transactions: The transactions to filter
     - parameter beforeAndIncludingDate: The date to compare against
     - returns: An array of all the provided transactions before and including the provided `beforeAndIncludingDate`
     */
    fileprivate func getMatchingTransactions(_ transactions: [Transaction], beforeAndIncludingDate date: Date) -> [Transaction]
    {
        return transactions.filter { !isLaterTransaction($0, thanDate: date) }
    }
    
    /**
     For a collection on `Transaction` objects, get all `Transactions` that occur after the current date
     
     - parameter transactions: The transactions to filter
     - parameter afterDate: The date to compare against
     - returns: An array of all the provided transactions after the provided `afterDate`
     */
    fileprivate func getMatchingTransactions(_ transactions: [Transaction], afterDate date: Date) -> [Transaction]
    {
        return transactions.filter { isLaterTransaction($0, thanDate: date) }
    }
    
    /**
     For a collection on `Transaction` objects, sort all `Transactions` by their date
     
     - parameter transactions: The transactions to sort
     - parameter isAscending: Specifies the direction of the returned array. `true` indicates the dates will go from earliest to latest.
     - returns: An array of all the provided transactions, sorted by date
     */
    fileprivate func sortTransactionsByDate(_   transactions: [Transaction],
                                                ascending: Bool) -> [Transaction]
    {
        let comparisonOrder : ComparisonResult = ascending ? .orderedAscending : .orderedDescending
        return transactions.sorted { $0.date.compare($1.date as Date) == comparisonOrder }
    }
    
    /**
     Indicates whether the given transaction's date is after the given date
     
     - parameter transaction: The transaction to check
     - parameter thanDate: The date to compare against
     - returns: A boolean, `true` if the transaction occurs after the given date, `false` if it is before or the same
     */
    fileprivate func isLaterTransaction(_ transaction: Transaction, thanDate: Date = Date()) -> Bool
    {
        return ((transaction.date as NSDate).laterDate(Date()) == transaction.date as Date)
    }
    
    /**
     Get the balance value for all current `Balance` object's transactions that have occurred before and including today
     
     - returns: The sum of past and current-day transactions' values or nil if no current `Balance`
     */
    func getCurrentBalance() -> Float?
    {
        return self.getPastTransactionsOrderedByDate()?.reduce(Float(0.0), addTransactionToSum)
    }
    /**
     Get the balance value for all current `Balance` object's transactions, including future ones
     
     - returns: The sum of all transactions' values or nil if no current `Balance`
     */
    func getProjectedBalance() -> Float?
    {
        return self.getTransactionsOrderedByDate()?.reduce(Float(0.0), addTransactionToSum)
    }
    
    /**
     Get the formatter used to convert currency properties from `Balance` totals into strings
     
     - returns: A `NSNumberFormatter` for displaying a transaction's currency-based information
     */
    func getCurrencyFormatter() -> NumberFormatter
    {
        let currencyFormatter = NumberFormatter()
        currencyFormatter.numberStyle = .currency
//      TODO: Add currency property to Balance
        currencyFormatter.currencyCode = Locale.current.currencyCode
        return currencyFormatter
    }
    
    /**
     Adds the given transactions value to a running sum
     
     - parameter sum: The running total
     - parameter transaction: The current transaction to add
     - returns: The new sum, including the tranaction's value
    */
    fileprivate func addTransactionToSum(_ sum: Float, transaction: Transaction) -> Float
    {
        return sum + transaction.value
    }
}
