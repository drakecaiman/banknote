//
//  Balance.swift
//  banknote-ios
//
//  Created by Duncan Oliver on 8/24/16.
//  Copyright © 2016 Highjacks. All rights reserved.
//

/// `Balance` objects are a collection of transactions whose order in time defined the total of the balance at any date
class Balance
{
//  MARK: Properties
//  The transactions belonging to this balance
    var transactions = [Transaction]()
    var currency = ""
}