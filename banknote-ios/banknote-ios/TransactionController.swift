//
//  TransactionController.swift
//  banknote-ios
//
//  Created by Duncan Oliver on 8/25/16.
//  Copyright © 2016 Highjacks. All rights reserved.
//

import Foundation

/**
 `TransactionController` provides transformed data based on the currently represented `Transaction`.
 */
class TransactionController
{
//  MARK: Properties
/// The current `Transaction` controlled
    var transaction : Transaction?
    
//  MARK: Initializers
    /**
     Create a new `TransactionController` for the given `Transaction
     
     - parameter withTransaction: The `Transaction` object to control
     
     - returns: A newly prepared `TransactionController` for controlling the specified `Transaction`
     */
    init(withTransaction transaction: Transaction? = nil)
    {
        self.transaction = transaction
    }
    
//  -
    /**
     Set the current `Transaction` object's name
     
     - parameter name: The new name value for the current `Transaction`
     */
    func setName(_ name: String)
    {
        self.transaction?.name = name
    }
    
    /**
     Set the current `Transaction` object's category
     
     - parameter name: The new category value for the current `Transaction`
     */
    func setCategory(_ category: String)
    {
        self.transaction?.category = category
    }
    
    /**
     Set the current `Transaction` object's terminus
     
     - parameter name: The new terminus value for the current `Transaction`
     */
    func setTerminus(_ terminus: String)
    {
        self.transaction?.terminus = terminus
    }
    
    /**
     Set the current `Transaction` object's date
     
     - parameter date: The new date value for the current `Transaction`
     */
    func setDate(_ date: Date)
    {
        self.transaction?.date = date
    }
    
    /**
     Set the current `Transaction` object's direction
     
     - parameter direction: The new direction value for the current `Transaction`
     */
    func setDirection(_ direction: Transaction.Direction)
    {
        self.transaction?.direction = direction
    }
    
    /**
     Set the current `Transaction` object's amount
     
     - parameter amount: The new amount value for the current `Transaction`
     */
    func setAmount(_ amount: Float)
    {
        self.transaction?.amount = amount
    }
    
    /**
     Get a string for the date from the transaction of this controller
     
     - returns: The formatted string representation of the date for the current transaction or nil if no transaction is controlled
     */
    func getDateString() -> String?
    {
        guard let dateValue = self.transaction?.date else { return nil }
        return self.getDateFormatter().string(from: dateValue)
    }
    
    /**
     Get the formatter used to convert dates from `Transaction` objects into strings
     
     - returns: A `NSDateFormatter` for displaying a transaction's date
     */
    func getDateFormatter() -> DateFormatter
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .short
        dateFormatter.timeStyle = .none
        return dateFormatter
    }
    
    /**
     Get a string for the amount from the transaction of this controller
     
     - returns: The formatted string representation of the amount for the current transaction. Returns `nil` in no transaction is controlled or if a string could not be constructed.
     */
    func getAmountString() -> String?
    {
        guard let transactionAmount = self.transaction?.amount else { return nil }
        return self.getCurrencyFormatter().string(from: transactionAmount as NSNumber)
    }
    
    /**
     Get a string for the value from the transaction of this controller
     
     - returns: The formatted string representation of the value for the current transaction. Returns `nil` in no transaction is controlled or if a string could not be constructed.
     */
    func getValueString() -> String?
    {
        guard let transactionValue = self.transaction?.value else { return nil }
        return self.getCurrencyFormatter().string(from: transactionValue as NSNumber)
    }
    
    /**
     Get the formatter used to convert currency properties from `Transaction` objects into strings
     
     - returns: A `NSNumberFormatter` for displaying a transaction's currency-based information
     */
    func getCurrencyFormatter() -> NumberFormatter
    {
        let currencyFormatter = NumberFormatter()
        currencyFormatter.numberStyle = .currency
        currencyFormatter.currencyCode = self.transaction?.currency
//        currencyFormatter.zeroSymbol = nil
        return currencyFormatter
    }
}
