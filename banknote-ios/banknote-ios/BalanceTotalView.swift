//
//  BalanceTotalView.swift
//  banknote-ios
//
//  Created by Duncan Oliver on 8/26/16.
//  Copyright © 2016 Highjacks. All rights reserved.
//

import UIKit

class BalanceTotalView: UITableViewHeaderFooterView
{
    @IBOutlet var descriptionLabel : UILabel?
    @IBOutlet var totalLabel : UILabel?
    
    func setDescription(_ description: String)
    {
        self.descriptionLabel?.text = description
    }
    
    func setTotal(_ total: String)
    {
        self.totalLabel?.text = total
    }
}
