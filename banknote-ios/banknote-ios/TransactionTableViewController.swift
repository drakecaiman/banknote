//
//  FirstViewController.swift
//  banknote-ios
//
//  Created by Duncan Oliver on 8/24/16.
//  Copyright © 2016 Highjacks. All rights reserved.
//

import UIKit

//  TODO: If structure of storing transactions in Balance changes, consider where new transaction show up when opening detail for addTransaction
//  TODO: Expanded display?
/// `TransactionTableViewController` displays a list of transactions for a given balance, ordered by date, separated by recorded transactions in the past, and planned transactions in the future
class TransactionTableViewController : UITableViewController
{
//  MARK: Constants
/// The segue identifier for transisting to the detail view of a transaction
    fileprivate let TRANSACTION_TABLE_VIEW_SEGUE_IDENTIFIER_DETAIL    = "TransactionDetailSegue"
    
/// The reuse identifier for the transaction display table cell
    fileprivate let TRANSACTION_TABLE_VIEW_CELL_NAME_TRANSACTION            = "TransactionTableViewCell"
/// The reuse identifier for the balance display table cell
    fileprivate let TRANSACTION_TABLE_VIEW_FOOTER_IDENTIFIER                = "TransactionTableViewFooter"
/// The desired height for the table section footer
    fileprivate let TRANSACTION_TABLE_VIEW_FOOTER_HEIGHT : CGFloat = 22.0
/// The total number of sections
    fileprivate let TRANSACTION_TABLE_VIEW_SECTION_COUNT = 2
/// `TransactionTableViewSection` defines the section indices used by the transaction table view
    fileprivate enum TransactionTableViewSection : Int
    {
///     The section for displaying transactions upto and including today's date
        case past
///     The section for displaying transactions after today's date
        case future
    }
    
//  MARK: Properties
/// The `BalanceController` used to grab information about the currently displayed balance
    fileprivate var balanceController : BalanceController? = BalanceController(with: Balance())
    
//  MARK: Actions
    /**
     Create a new transaction for the current balance and update the display
     
     - parameter sender: The object sending the action
     */
    @IBAction func addTransaction(_ sender: AnyObject)
    {
//      Create a new transactions
        self.balanceController?.newTransaction()
//      Update display
        self.tableView.beginUpdates()
        let pastSectionIndex = TransactionTableViewSection.past.rawValue
        let numberOfPastRows = self.tableView.numberOfRows(inSection: pastSectionIndex)
        self.tableView.insertRows(at:   [[pastSectionIndex, numberOfPastRows]],
                                  with: .fade)
        self.tableView.endUpdates()
        self.performSegue(withIdentifier:   TRANSACTION_TABLE_VIEW_SEGUE_IDENTIFIER_DETAIL,
                          sender:           self.balanceController?.balance?.transactions.last)
    }
    
    /**
     Unwind from an editing view
     
     - parameter sender: The unwinding segue
     */
    @IBAction func returnFromEdit(_ sender: UIStoryboardSegue)
    {
        self.tableView.reloadData()
    }
    
//  MARK: UIViewController methods
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        
        let balanceViewNib = UINib(nibName: "BalanceView", bundle: nil)
        self.tableView.register(balanceViewNib,
                                forHeaderFooterViewReuseIdentifier: TRANSACTION_TABLE_VIEW_FOOTER_IDENTIFIER)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
//      Send the transaction displayed by the tapped row to the detail view
        if let destinationTransactionViewController = segue.destination as? TransactionDetailViewController,
            let tappedTransaction = sender as? Transaction,
            segue.identifier == TRANSACTION_TABLE_VIEW_SEGUE_IDENTIFIER_DETAIL
        {
            destinationTransactionViewController.setTransaction(tappedTransaction)
        }
    }
    
//  MARK: UITableViewDelegate methods
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        return CGFloat.leastNormalMagnitude
    }

    override func tableView(_ tableView: UITableView,
                            heightForFooterInSection section: Int) -> CGFloat
    {
//      Check for expected section
        guard let _ = TransactionTableViewSection(rawValue: section)
            else
        {
            print("Unexpected section")
            return CGFloat.leastNormalMagnitude
        }
        return self.tableView(tableView, numberOfRowsInSection: section) > 0 ? TRANSACTION_TABLE_VIEW_FOOTER_HEIGHT : CGFloat.leastNormalMagnitude
    }
    
    override func tableView(_                       tableView: UITableView,
                            viewForFooterInSection  section: Int) -> UIView?
    {
//      Grab reusable cell for footer
        guard let reuseTableViewFooter = tableView.dequeueReusableHeaderFooterView(withIdentifier: TRANSACTION_TABLE_VIEW_FOOTER_IDENTIFIER) as? BalanceTotalView else { return nil }

        let sectionCount            : Int
        let balanceDescription      : String
        let balanceValue            : Float
//      Check for expected section
        guard let currentSection = TransactionTableViewSection(rawValue: section)
            else
        {
            print("Unexpected section")
            return nil
        }
        switch currentSection
        {
        case .past:
            sectionCount = self.balanceController?.getPastTransactionsOrderedByDate()?.count ?? 0
            balanceDescription = NSLocalizedString("TransactionTableViewBalanceLabelCurrent",
                                                   comment: "The main label for displaying the sum of all transactions up to and including the current date")
            balanceValue = self.balanceController?.getCurrentBalance() ?? 0.0
        case .future:
            sectionCount = self.balanceController?.getFutureTransactionsOrderedByDate()?.count ?? 0
            balanceDescription = NSLocalizedString("TransactionTableViewBalanceLabelFuture",
                                                   comment: "The main label for displaying the sum of all transactions, including future ones")
            balanceValue = self.balanceController?.getProjectedBalance() ?? 0.0
        }
        guard sectionCount > 0 else { return nil }
        reuseTableViewFooter.descriptionLabel?.text = balanceDescription
        reuseTableViewFooter.totalLabel?.text = self.balanceController?.getCurrencyFormatter().string(from: balanceValue as NSNumber)
    
        return reuseTableViewFooter
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        tableView.beginUpdates()
        tableView.endUpdates()
    }
    
    override func tableView(_                               tableView: UITableView,
                            accessoryButtonTappedForRowWith indexPath: IndexPath)
    {
        let tappedTransaction : Transaction?
//      Check for expected section
        guard let currentSection = TransactionTableViewSection(rawValue: indexPath.section)
            else
        {
            print("Unexpected section")
            return
        }
        switch currentSection
        {
        case .past:
            tappedTransaction = balanceController?.getPastTransactionsOrderedByDate()?[indexPath.row]
        case .future:
            tappedTransaction = balanceController?.getFutureTransactionsOrderedByDate()?[indexPath.row]
        }
        self.performSegue(withIdentifier: TRANSACTION_TABLE_VIEW_SEGUE_IDENTIFIER_DETAIL, sender: tappedTransaction)
    }
    
//  MARK: UITableViewDataSource methods
    override func numberOfSections(in tableView: UITableView) -> Int
    {
        return TRANSACTION_TABLE_VIEW_SECTION_COUNT
    }

    override func tableView(_                       tableView: UITableView,
                            numberOfRowsInSection   section: Int) -> Int
    {
//      Check for expected section
        guard let currentSection = TransactionTableViewSection(rawValue: section)
            else
        {
            print("Unexpected section")
            return 0
        }
        switch currentSection
        {
        case .past:
            guard let pastBalanceTransactions = balanceController?.getPastTransactionsOrderedByDate() else { break }
            return pastBalanceTransactions.count
        case .future:
            guard let futureBalanceTransactions = balanceController?.getFutureTransactionsOrderedByDate() else { break }
            return futureBalanceTransactions.count
        }
        
        return 0
    }
    
    override func tableView(_               tableView: UITableView,
                            cellForRowAt    indexPath: IndexPath) -> UITableViewCell
    {
//      Get the table view cell
        let reuseTableViewCell = tableView.dequeueReusableCell(withIdentifier:  TRANSACTION_TABLE_VIEW_CELL_NAME_TRANSACTION,
                                                               for:             indexPath)
        
        let transactionArray : [Transaction]?
//      Check for expected section
        guard let currentSection = TransactionTableViewSection(rawValue: indexPath.section)
            else
        {
            print("Unexpected section")
            return reuseTableViewCell
        }
        switch currentSection
        {
        case .past:
            transactionArray = balanceController?.getPastTransactionsOrderedByDate()
        case .future:
            transactionArray = balanceController?.getFutureTransactionsOrderedByDate()
        }
//      Get the corresponding transaction
        if let currentTransaction = transactionArray?[indexPath.row]
        {
            let transactionController = TransactionController(withTransaction: currentTransaction)
            if let transactionTableViewCell = reuseTableViewCell as? TransactionTableViewCell
            {
                transactionTableViewCell.nameLabel?.text        = transactionController.transaction?.name
                transactionTableViewCell.categoryLabel?.text    = transactionController.transaction?.category
                transactionTableViewCell.terminusLabel?.text    = transactionController.transaction?.terminus
                transactionTableViewCell.valueLabel?.text       = transactionController.getValueString()
            }
            else
            {
                reuseTableViewCell.textLabel?.text          = transactionController.transaction?.name
                reuseTableViewCell.detailTextLabel?.text    = transactionController.getValueString()
            }
        }
        
        return reuseTableViewCell
    }
}

